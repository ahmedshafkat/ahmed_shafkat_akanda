from ast import If
import time
time.sleep(3) # waiting for displax to reboot
from select import select
from multiprocessing import Queue
import os
from evdev import InputDevice, ecodes
from multiprocessing import Process
import threading
import socket, sys
from xmlrpc.server import SimpleXMLRPCServer
from apa102_pi.driver import apa102
import random
import datetime
import sunrise
import math

touch_ui_q_p1 = Queue()
touch_ui_q_p2 = Queue()
touch_ui_q_p3 = Queue()

i=0
NewData = False
NUM_LED = 720
mosi = 10
sclk = 11
mode = 2
procUpdate = [None] * 54
busSpeed = 500000

strip = apa102.APA102(num_led=NUM_LED, order='rgb', mosi=mosi, sclk=sclk, bus_speed_hz=busSpeed)
strip_boot_up = apa102.APA102(num_led=720, order='rgb', mosi=mosi, sclk=sclk, bus_speed_hz=busSpeed)
strip1 = apa102.APA102(num_led=NUM_LED, order='rgb', bus_speed_hz=busSpeed)

colorTemp = [0] * len(strip.leds)
ledIndex = [-1] * len(strip.leds)


devNames = [
    "/dev/input/by-id/usb-DISPLAX_SKIN_DUALTOUCH__3d1264ce1fcf2c8f7f8863d7be8aa9506c43779d__0C2025000340-event-if00",
    "/dev/input/by-id/usb-DISPLAX_SKIN_DUALTOUCH__3d1264ce1fcf2c8f7f8863d7be8aa9506c43779d__0C2025000336-event-if00",
    "/dev/input/by-id/usb-DISPLAX_SKIN_DUALTOUCH__3d1264ce1fcf2c8f7f8863d7be8aa9506c43779d__0C2025000329-event-if00",
    "/dev/input/by-id/usb-DISPLAX_SKIN_DUALTOUCH__3d1264ce1fcf2c8f7f8863d7be8aa9506c43779d__0C2025000330-event-if00"
]
factory_x = 2600
factory_y = 1450
calibrated_x = 2555
calibrated_y = 1327
x_pixel_per_mm = factory_x/728
y_pixel_per_mm = factory_y/432
x_spacing = 10*x_pixel_per_mm
y_spacing = 23*y_pixel_per_mm
y_distance_from_edge = 30*y_pixel_per_mm
x_distance_from_edge = 20*x_pixel_per_mm
x_rows = 6
y_rows = 3
x_total_max = calibrated_x #factory_x #- x_distance_from_edge
x_total_min = 0 #+ y_distance_from_edge
x_touch_btn_size = 106.7*x_pixel_per_mm#((x_total_max - x_total_min) - (spacing * (x_rows + 1))) / x_rows
y_total_max = calibrated_y #factory_y #- y_distance_from_edge
y_total_min = 0 #+ y_distance_from_edge
y_touch_btn_size = 113.3*y_pixel_per_mm#((y_total_max - y_total_min) - (spacing * (y_rows + 1))) / y_rows
#units should be calculated :
# x = 725mm 2600 pixels
# y = 430mm 1450 pixels
# x distance from edge to field = 20mm
# y distance from edge to field = 30mm
# x spacing in between fields is 10mm
# y spacing in between fields is 23mm



limits = [
    {
        '1': {'lower_x_limit': (x_total_max - x_touch_btn_size), 'upper_x_limit': x_total_max}, # total max should be minus a spacing
        '2': {'lower_x_limit': (x_total_max - 2*x_touch_btn_size - x_spacing), 'upper_x_limit': (x_total_max - x_touch_btn_size - x_spacing)},
        '3': {'lower_x_limit': (x_total_max - 3*x_touch_btn_size - 2*x_spacing), 'upper_x_limit': (x_total_max - 2*x_touch_btn_size - 2*x_spacing)},
        '4': {'lower_x_limit': (x_total_min + 2*x_touch_btn_size + 2*x_spacing), 'upper_x_limit': (x_total_min + 3*x_touch_btn_size + 2*x_spacing)},
        '5': {'lower_x_limit': (x_total_min + x_touch_btn_size + x_spacing), 'upper_x_limit': (x_total_min + (2*x_touch_btn_size) + x_spacing)},
        '6': {'lower_x_limit': x_total_min, 'upper_x_limit': (x_total_min + x_touch_btn_size)},
        'c': {'lower_y_limit': (y_total_max - y_touch_btn_size), 'upper_y_limit': y_total_max},  
        'b': {'lower_y_limit': (y_total_min + y_touch_btn_size + y_spacing), 'upper_y_limit': (y_total_min + 2*y_touch_btn_size + y_spacing)},
        'a': {'lower_y_limit': y_total_min, 'upper_y_limit': (y_total_min + y_touch_btn_size)},

    }
]
print(f"hello world {limits}")

def get_user_input(grid_q: Queue, dev_name, panel_id=000, verbose=False, getvalue=False):
    try:
        ''' Get user input from the grid and send it to the UI '''
        x = 0
        y = 0
        dev = InputDevice(os.path.realpath(dev_name))
        my_limits = limits[0]

        ret_vals = [
            [63, 53, 43, 33, 23, 13],
            [62, 52, 42, 32, 22, 12],
            [61, 51, 41, 31, 21, 11],
        ]

        while True:
            try:
                _, _, _ = select([dev], [], [])
            except KeyboardInterrupt:
                break

            for event in dev.read():
                if event.code in [ecodes.BTN_TOUCH] and event.value == 0:
                    a, b = -1, -1
                    UI_input = -1
                    if(my_limits["a"]['lower_y_limit'] < y < my_limits["a"]['upper_y_limit']):
                        a = 0
                    elif(my_limits["b"]['lower_y_limit'] < y < my_limits["b"]['upper_y_limit']):
                        a = 1
                    elif(my_limits["c"]['lower_y_limit'] < y < my_limits["c"]['upper_y_limit']):
                        a = 2

                    if(my_limits["1"]['lower_x_limit'] < x < my_limits["1"]['upper_x_limit']):
                        b = 5
                    elif(my_limits["2"]['lower_x_limit'] < x < my_limits["2"]['upper_x_limit']):
                        b = 4
                    elif(my_limits["3"]['lower_x_limit'] < x < my_limits["3"]['upper_x_limit']):
                        b = 3
                    if(my_limits["4"]['lower_x_limit'] < x < my_limits["4"]['upper_x_limit']):
                        b = 2
                    elif(my_limits["5"]['lower_x_limit'] < x < my_limits["5"]['upper_x_limit']):
                        b = 1
                    elif(my_limits["6"]['lower_x_limit'] < x < my_limits["6"]['upper_x_limit']):
                        b = 0

                    if(a != -1 and b != -1):
                        UI_input = ret_vals[a][b] #+ panel_id
                        grid_q.put(UI_input)

                    if verbose:
                        print(f"Presssed at x:{x}\ty:{y}\t Grid val: {UI_input + panel_id},  {UI_input}::{panel_id},  a:{a}\tb:{b}\t")

                    if getvalue:
                        return[x,y]
                elif event.type in [ecodes.EV_ABS]:
                    if event.code in [ecodes.ABS_MT_SLOT, ecodes.ABS_MT_POSITION_X, ecodes.ABS_MT_POSITION_Y, ecodes. ABS_MT_TRACKING_ID]:
                        #print(f"{ecodes.bytype[event.type][event.code]}: {event.value}")
                        if (event.code == ecodes.ABS_MT_POSITION_X):
                            x = event.value
                        elif(event.code == ecodes.ABS_MT_POSITION_Y):
                            y = event.value
    except FileNotFoundError:
        pass


def RPCServer():
    global commandQueue

    server = SimpleXMLRPCServer(("", 50000), allow_none=True, logRequests=False)
    server.RequestHandlerClass.protocol_version = "HTTP/1.1"
    server.register_function(control_led, "control_led")
    server.register_function(control_all_led, "control_all_led")
    server.register_function(clear_led, "clear_led")
    server.register_function(clear_led_boot_up, "clear_led_boot_up")
    server.register_function(ui_get_next_p1, "ui_get_next_p1")
    server.register_function(ui_get_next_p2, "ui_get_next_p2")
    server.register_function(ui_get_next_p3, "ui_get_next_p3")
    server.register_function(ui_get_next_all, "ui_get_next_all")
    server.register_function(show_leds_thread, "show_leds")
    server.register_function(blink, "blink")
    server.register_function(ui_clear, "ui_clear")
    server.register_function(blink_invalid, "blink_invalid")
    server.register_function(random_LED, "random_LED")
    server.register_function(compareList, "compareList")
    server.register_function(daycycle,"daycycle")

    print("RPCServer is running")
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        strip.clear()
        strip.cleanup()
        pass

def daycycle(hour, minute, second, offset):
    
    offsetmin=0
    offsethours = 0

    if(offset>0):
        if(offset>=60):
            offsethours = math.floor(offset/60)
            offsetmin = offset%60
        else:
            offsetmin = offset
    elif(offset<0):
        if(offset>=-60):
            offsethours = math.floor(offset/60)
            offsetmin = offset%60
        else:
            offsetmin = offset


    offsetUp = datetime.datetime.now(tz=datetime.timezone(datetime.timedelta(hours=2+offsethours,minutes=offsetmin)))
    offsetDown = datetime.datetime.now(tz=datetime.timezone(datetime.timedelta(hours=2-offsethours,minutes=-offsetmin)))
    sUp = sunrise.Sun(offsetUp, 55.4, 10.4)
    sDown = sunrise.Sun(offsetDown, 55.4, 10.4)
    sunUp = sUp.sunrise()
    sunDown = sDown.sunset()
    
    now = datetime.time(hour, minute, second)
    print(now)
    print(sunDown)
    print(sunUp)

    if(now < sunDown and now > sunUp):
        res = "day"
        return res
    else:
        res = "night"
        return res



def random_LED(x):
    random_nr = random.sample([21, 22, 23, 31, 32, 33, 41, 42, 43, 51, 52, 53, 61, 62, 63], x)

    return random_nr

def compareList(randLed,userIn,round):
    randList = randLed[0:round]
    userInput = userIn[0:round]
    if(randList == userInput):
        res = "green"
        return res
    else:
        res = "red"
        return res

def control_led(conmmandID):
    global strip
    global strip1
    global NewData
 # functions on the robot dti_set_led should be handled on the pi
 # and list to led should be handled on the pi

    led_id_vals =[[10, 17, 24, 31, 38, 45],
                  [105, 98, 91, 84, 77, 70],
                  [130, 137, 144, 151, 158, 165]]


    # maybe needs to take a list of lists as input instead and parse it down to a commandID, to have it running
    # on the raspberry pi
    commandID_split = [int(a) for a in str(conmmandID)]
    #fieldID = (led_id_vals[commandID_split[0] - 1][commandID_split[2]-1][commandID_split[1]-1])
    action = commandID_split[3]
    r = commandID_split[4]
    g = commandID_split[5]
    b = commandID_split[6]
    s = int(36)

    no_led_per_field = 7
    #offset= (no_led_per_field * (fieldID-1))
    offset= led_id_vals[commandID_split[2]-1][commandID_split[1]-1] + 240*(commandID_split[0]-1)-1
    # print(f"Robot sends:\t{offset}")
    #print(f"{fieldID} check!")
    # print(f"{strip.leds}\n")
    for i in range(offset, offset + no_led_per_field):
        merge_buffer(i, r*s, g*s, b*s, action)
       # if offset != 164:
    # print(f"control led, strips: {strip.leds}\n")
    # print(f"control led, strips1: {strip1.leds}\n")


    NewData = True
    # print("NewData assigned TRUE")
    return offset

def control_all_led(list_conmmandID):
    for i in range(len(list_conmmandID)):
        # print(list_conmmandID[i], " list_command[", i,"]")
        control_led(list_conmmandID[i])
        i = i + 1

def merge_buffer(index, r, g, b, action):
        global strip, strip1
        reg_hex = strip.combine_color(r, g, b)
        strip.set_pixel_rgb(index, reg_hex)
        j = 0

        # while j < len(strip.leds):
        #     if strip1.leds[j] != 0 and (j % 4) != 0 and strip1.leds[j] != strip.leds[j]:
        #         ledIndex[j] = action
        #         #print(j)
        #         if (j % 4) == 1:
        #             strip.leds[j] = strip1.leds[j]
        #             strip.leds[j+1] = strip1.leds[j+1]
        #             strip.leds[j+2] = strip1.leds[j+2]
        #             ledIndex[j] = action
        #             ledIndex[j+1] = action
        #             ledIndex[j+2] = action

        #         elif (j % 4) == 2:
        #             strip.leds[j-1] = strip1.leds[j-1]
        #             strip.leds[j] = strip1.leds[j]
        #             strip.leds[j+1] = strip1.leds[j+1]
        #             ledIndex[j-1] = action
        #             ledIndex[j] = action
        #             ledIndex[j+1] = action

        #         elif (j % 4) == 3:
        #             strip.leds[j-2] = strip1.leds[j-2]
        #             strip.leds[j-1] = strip1.leds[j-1]
        #             strip.leds[j] = strip1.leds[j]
        #             ledIndex[j-2] = action
        #             ledIndex[j-1] = action
        #             ledIndex[j] = action



        #     j = j + 1
        # #show_leds()

def show_leds_thread():
    global strip
    global NewData
    global i

    # print("Show LED started")

    while True:
        if NewData:
            strip.show()
            NewData = False
           # print(f"Strips: {strip.leds}")
           # print("new data set to false")

    #     i=i+1
    #     if i>50000:
    #         i=0
    # #print(ledIndex)
    #print("Show!")

def blink(blinkType):
        j = 0
        #print(ledIndex)
        while j < len(strip.leds):
            if ledIndex[j] == blinkType:
                if strip.leds[j] != 0:
                    colorTemp[j] = strip.leds[j]
                    strip.leds[j] = 0
                else:
                    strip.leds[j] = colorTemp[j]
            j = j + 1

def blink_invalid():
        j = 0
        while j < len(strip.leds):
            if ledIndex[j] == 3:
                if strip.leds[j] != 0:
                    strip.leds[j] = 0
                ledIndex[j] = -1
            j = j + 1

def clear_led():
    # j = 0
    # while j < len(strip.leds):
    #     if ledIndex[j] != -1:
    #         strip.leds[j] = 0
    #         strip1.leds[j] = 0
    #         ledIndex[j] = -1
    #     j = j + 1
    strip.clear_strip()

def clear_led_boot_up():
    # j = 0
    # while j < len(strip.leds):
    #     if ledIndex[j] != -1:
    #         strip.leds[j] = 0
    #         strip1.leds[j] = 0
    #         ledIndex[j] = -1
    #     j = j + 1
    strip_boot_up.clear_strip()


def ui_get_next_p1():
    #global i

    if not touch_ui_q_p1.empty():
        a = touch_ui_q_p1.get()
        print(a)
        return a
    else:
        return 0

def ui_get_next_p2():
    if not touch_ui_q_p2.empty():
        a1 = touch_ui_q_p2.get()
        print(a1)
        return a1
    else:
        return 0

def ui_clear():
    while not touch_ui_q_p1.empty():
       tmp = touch_ui_q_p1.get()
       print("empty queue1")
    while not touch_ui_q_p2.empty():
       print("empty queue2")
       tmp = touch_ui_q_p2.get()
    while not touch_ui_q_p3.empty():
       print("empty queue3")
       tmp = touch_ui_q_p3.get()
    
def ui_get_next_p3():
    if not touch_ui_q_p3.empty():
        a2 = touch_ui_q_p3.get()
        print(a2)
        return a2
    else:
        return 0

def ui_get_next_all():
    #print("next all start")
    return [0, ui_get_next_p1(), ui_get_next_p2(), ui_get_next_p3()]
    #return [0,0,0,0]

def Start_LED():
    for i in range(720):
        reg_hex = strip_boot_up.combine_color(0, 255, 0)
        strip_boot_up.set_pixel_rgb(i, reg_hex)
        i = i + 1
    strip_boot_up.show()     

if __name__ == "__main__":
    
    # user friendly, turns off all LEDs and turns them green 
    # after everything is ready to press the external "reset"
    # btn on robot.
    clear_led()
    Start_LED()
    
    #get_user_input(master_queue, devNames[2], verbose=True)
    thread_1 = threading.Thread(target=get_user_input, args=[touch_ui_q_p1, devNames[0], 100, True,])
    thread_1.start()

    thread_2 = threading.Thread(target=get_user_input, args=[touch_ui_q_p2, devNames[1], 200, True,])
    thread_2.start()

    thread_3 = threading.Thread(target=get_user_input, args=[touch_ui_q_p3, devNames[3], 300, True,])
    thread_3.start()

    thread_4 = threading.Thread(target=show_leds_thread)
    thread_4.start()

    # proc1 = Process(target=get_user_input, args=[touch_ui_q_p1, devNames[0], 100, True,])
    # proc1.start()

    # proc2 = Process(target=get_user_input, args=[touch_ui_q_p2, devNames[1], 200, True,])
    # proc2.start()

    # proc3 = Process(target=get_user_input, args=[touch_ui_q_p3, devNames[3], 300, True,])
    # proc3.start()


    # proc4 = Process(target=show_leds)
    # proc4.start()
    #proc4.join()

    RPCServer()
